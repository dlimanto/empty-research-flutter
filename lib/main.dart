import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  final data = [
    { 'text': 'Sawi', 'category': 'Sayur' },
    { 'text': 'Bayam', 'category': 'Sayur' },
    { 'text': 'Brokoli', 'category': 'Sayur' },
    { 'text': 'Kol', 'category': 'Sayur' },
    { 'text': 'Dada', 'category': 'Ayam' },
    { 'text': 'Paha atas', 'category': 'Ayam' },
    { 'text': 'Sayap', 'category': 'Ayam' },
    { 'text': 'Paha bawah', 'category': 'Ayam' },
    { 'text': 'Ceker', 'category': 'Ayam' },
    { 'text': 'Tenderloin', 'category': 'Daging' },
    { 'text': 'Sirloin', 'category': 'Daging' },
    { 'text': 'Rib eye', 'category': 'Daging' },
    { 'text': 'Short plate', 'category': 'Daging' }
  ];

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    print('Build My App');
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Row(
        children: [
          Container(
            width: 80,
            color: Colors.red,
            height: double.infinity,
            child: SingleChildScrollView(
              child: Column(
                children: const [
                  Text('Sayur'),
                  SizedBox(height: 16),
                  Text('Ayam'),
                  SizedBox(height: 16),
                  Text('Daging'),
                ],
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, i) {
                print('Cetak $i: ${data[i]['text']}');
                
                if (i == 0 || data[i]['category'] != data[i - 1]['category']) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      const SizedBox(height: 16),
                      Text(data[i]['category']?? '', style: Theme.of(context).textTheme.headline5),
                      const SizedBox(height: 16),
                      Container(
                        color: Colors.blue,
                        padding: const EdgeInsets.all(16),
                        margin: const EdgeInsets.all(16),
                        child: Text(data[i]['text']?? ''),
                      )
                    ],
                  );
                }

                return Container(
                  color: Colors.blue,
                  padding: const EdgeInsets.all(16),
                  margin: const EdgeInsets.all(16),
                  child: Text(data[i]['text']?? ''),
                );
              }
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
